
public class Car extends Automobile {
	public Car(){
		this.setBodyStyle("Car");
		this.setBrand("Unknown Brand");
	}
	public Car(String brand){
		this.setBodyStyle("Car");
		this.setBrand(brand);
	}
	public void openTrunk(){
		System.out.println("Opened the trunk of the " + this.getBrand() + " " + this.getBodyStyle());
	}
	public void closeTrunk(){
		System.out.println("Closed the trunk of the " + this.getBrand() + " " + this.getBodyStyle());
	}
}
